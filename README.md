# Finatra Http-Thrift Archetype

### Once Write Use 2-Way
- Http : /sample?id=1&name=kim
- Thrift : client.sample(1, "kim")

##### Use
```
  override val [Thrift Interface] =
    _get([route], handle([ThriftMethod]))
    { [Request Parameter] => [ThriftMethod.Args] }
    { [ThriftMethod.Args] =>  
        ...(implement)...
    }
```
##### Example 
- [ExampleController.scala](/src/main/scala/example/ExampleController.scala#L24-L31)
```scala
  override val sample =
    _get("/sample", handle(Sample))
    { (params : Params) => Sample.Args(params.id, params.name) }
    { (args : Sample.Args) =>
      ExampleService
        .get(args.id, args.name)
        .map(toTDto)
    }
```    
