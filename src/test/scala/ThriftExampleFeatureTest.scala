import com.twitter.finatra.thrift.EmbeddedThriftServer
import com.twitter.inject.server.FeatureTest
import com.twitter.util.Future
import example.thriftscala.{ExampleController, TExampleDto}

/**
  * Created by karellen on 2017. 1. 29..
  */
class ThriftExampleFeatureTest extends FeatureTest {

  override val server = new EmbeddedThriftServer(new Server)

  val client = server.thriftClient[ExampleController[Future]](clientId = "client123")

  "server" should {
    "add" in {
      val dto = client.sample(1, "kim").value
      client.sample(1, "kim").value should equal(TExampleDto(2, "kim!!"))
      client.sample(2, "lee").value should equal(TExampleDto(3, "lee!!"))
    }
  }

}
