import com.twitter.finagle.http.Status
import com.twitter.finatra.http.EmbeddedHttpServer
import com.twitter.inject.server.FeatureTest

/**
  * Created by karellen on 2017. 1. 29..
  */
class HttpExampleFeatureTest extends FeatureTest {

  override val server = new EmbeddedHttpServer(new Server)

  "server" should {
    "add" in {
      server.httpGet(path = "/sample?id=1&name=Gong-Yu",
        andExpect = Status.Ok,
        withBody = """{"id":2,"name":"Gong-Yu!!","passthrough_fields":{}}""".stripMargin
      )
    }
  }
}
