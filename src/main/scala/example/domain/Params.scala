package example.domain

import com.twitter.finatra.request.QueryParam

/**
  * Created by karellen on 2017. 1. 29..
  */
case class Params(@QueryParam id : Int,
                  @QueryParam name: String)
