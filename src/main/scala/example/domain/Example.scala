package example.domain

/**
  * Created by karellen on 2017. 1. 29..
  */
case class Example(id: Int, name: String)
