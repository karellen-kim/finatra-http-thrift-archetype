package example

import com.twitter.util.Future
import example.domain.Example

/**
  * Created by karellen on 2017. 1. 29..
  */
object ExampleService {
  def get(id: Int, name: String): Future[Example] =
    Future.value(Example(id + 1, s"${name}!!"))
}
