package example

import com.twitter.finagle.http.Request
import core.Controller
import example.domain.{Example, Params}
import example.thriftscala.ExampleController.Sample
import example.thriftscala.{ExampleController, TExampleDto}

/**
  * Created by karellen on 2017. 1. 29..
  */
class ExampleController extends Controller[Sample.type] with ExampleController.BaseServiceIface {

  get("/") { request: Request =>
    response.temporaryRedirect.location("/sample?id=1&name=Gong-Yu")
  }

  /**
    * Support at once
    * 1. Http : /sample?id=1&name=kim
    * 2. Thrift : client.sample(1, "kim")
    */

  override val sample =
    _get("/sample", handle(Sample))
    { (params : Params) => Sample.Args(params.id, params.name) }
    { (args : Sample.Args) =>
      ExampleService
        .get(args.id, args.name)
        .map(toTDto)
    }

  implicit def toTDto(org: Example): TExampleDto = TExampleDto(org.id, org.name)
}