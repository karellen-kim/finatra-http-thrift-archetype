package core

import com.twitter.finatra.thrift.internal.ThriftMethodService
import com.twitter.finatra.{http, thrift}
import com.twitter.scrooge.{ThriftMethod, ToThriftService}

/**
  * Created by karellen on 2017. 1. 29..
  */
abstract class Controller[TService <: ThriftMethod] extends http.Controller with thrift.Controller with ToThriftService {

  def _get[Params : Manifest, Result : Manifest]
  (route: String, method : TService#FunctionType => ThriftMethodService[TService#Args, TService#Result])
  (conv : Params => TService#Args)
  (func : (TService#Args => Result) with TService#FunctionType) = {
    get(route)((params: Params) => func(conv(params)))
    method(func)
  }

}
