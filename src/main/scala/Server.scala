import com.twitter.finatra.http.HttpServer
import com.twitter.finatra.thrift.ThriftServer
import com.twitter.finatra.http.routing.HttpRouter
import com.twitter.finatra.thrift.routing.ThriftRouter
import example.ExampleController

/**
  * Created by karellen on 2017. 1. 28..
  */

object ServerMain extends Server

class Server extends HttpServer with ThriftServer {

  override def defaultFinatraHttpPort: String = ":8080"
  override val defaultFinatraThriftPort: String = ":9090"

  override def configureHttp(router: HttpRouter): Unit = {
    router
      .add[ExampleController]
  }

  override def configureThrift(router: ThriftRouter): Unit = {
    router
      .add[ExampleController]
  }
}