namespace java example.thriftjava
#@namespace scala example.thriftscala
namespace rb ExampleService

struct TExampleDto {
    1: required i32 id;
    2: required string name;
}

service ExampleController {
  TExampleDto sample(
    1: i32 id
    2: string name
  )
}